case class Ocena(
  imię: String,
  nazwisko: String,
  wdzięk: Int,
  spryt: Int
) {
  require(
    // upewniamy się, że składowe Oceny są sensowne
    imię.strip() != "" &&
    nazwisko.strip() != "" &&
    (0 to 20).contains(wdzięk) &&
    (0 to 20).contains(spryt)
  )
}

case class Wynik(
  miejsce: Int,
  imię: String,
  nazwisko: String,
  średniWdzięk: Double,
  średniSpryt: Double,
  suma: Double
) {
  // upewniamy się, że składowe Wyniku są „sensowne”
  require(
    miejsce >= 0 &&
    imię.strip() != "" &&
    nazwisko.strip() != "" &&
    średniWdzięk >= 0 && średniWdzięk <= 20 &&
    średniSpryt >= 0 && średniSpryt <= 20 &&
    suma == średniWdzięk + średniSpryt
  )
}

@main def zad_7_3: Unit = {
  val scores = List(
    Ocena("a", "b", 2, 4),
    Ocena("a", "b", 5, 10),
    Ocena("c", "d", 10, 8),
    Ocena("e", "f", 15, 15),
    Ocena("a", "b", 11, 13),
    Ocena("e", "f", 16, 14),
    Ocena("g", "h", 6, 7),
    Ocena("i", "j", 18, 18),
    Ocena("c", "d", 18, 19),
    Ocena("k", "l", 6, 7),
    Ocena("m", "n", 10, 17)
  )

  val results = scores.groupBy(osoba => (osoba.imię, osoba.nazwisko))
    .map(os => {
      val points = os._2  // lista z ocenami
      val amount = points.length
      val avgWdzięk = points.map(_.wdzięk).sum / amount
      val avgSpryt = points.map(_.spryt).sum / amount
      val avgSum = avgWdzięk + avgSpryt

      Wynik(0, os._1._1, os._1._2, avgWdzięk, avgSpryt, avgSum)
    })
    .toList
    .sortWith((o1, o2) => {
      if (o1.suma == o2.suma && o1.średniWdzięk == o2.średniWdzięk) o1.nazwisko < o2.nazwisko
      else if (o1.suma == o2.suma && o1.średniWdzięk != o2.średniWdzięk) o1.średniWdzięk > o2.średniWdzięk
      else o1.suma > o2.suma
    })
    .zip((1 to scores.length).toList)
    .foldLeft(List[(Wynik, Int)]()) {
      case (acc, (os, i)) =>
        if (i == 1) {
          acc :+ (os, i)
        } else {
          if (os._6 == acc.last._1._6) {
            acc :+ (os, acc.last._2)
          } else {
            acc :+ (os, acc.last._2 + 1)
          }
        }
    }
    .map((os, id) => Wynik(id, os._2, os._3, os._4, os._5, os._6))
  
  println(results)
}